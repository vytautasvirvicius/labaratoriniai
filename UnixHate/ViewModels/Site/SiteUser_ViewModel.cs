﻿using UnixHate.Models;

namespace UnixHate.ViewModels
{
    public class SiteUser_ViewModel
    {
        public string Message { get; set; }
        public Customer Customer { get; set; }
        public Encrypt Encrypt { get; set; }

        public SiteUser_ViewModel()
        {
            Message = "";
            Customer = new Customer();
            Encrypt = new Encrypt();
        }


    }

}