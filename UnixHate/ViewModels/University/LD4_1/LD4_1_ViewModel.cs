﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnixHate.Models.University.LD4_1;

namespace UnixHate.ViewModels.University.LD4_1
{
    public class LD4_1_ViewModel
    {
        public string Message = "";
        public string Data;
        public string Bank;
        public string FileNameItems = "~/L4_5_Data1/";
        public string FileNameWorkers = "~/L4_5_Data2/";
        public List<Item> Items = new List<Item>();
        public List<Worker> Workers = new List<Worker>();
        public List<Transaction> Transactions = new List<Transaction>();

        public void ReadWorkerData()
        {
            var csvFiles = Directory.EnumerateFiles(System.Web.HttpContext.Current.Server.MapPath(FileNameWorkers),
                "*.csv");
            foreach (var currentFile in csvFiles)
            {
                StreamReader file = new StreamReader(currentFile);
                while (!file.EndOfStream)
                {
                    string[] lines = file.ReadLine().Split(';');
                    Workers.Add(new Worker(lines[0], lines[1], lines[2], lines[3]));
                }
                file.Close();
            }
        }

        public void ReadItemData()
        {
            var csvFiles = Directory.EnumerateFiles(System.Web.HttpContext.Current.Server.MapPath(FileNameItems),
                "*.csv");
            foreach (var currentFile in csvFiles)
            {
                StreamReader file = new StreamReader(currentFile);
                var ignore = file.ReadLine();
                while (!file.EndOfStream)
                {
                    string[] lines = file.ReadLine().Split(';');
                    Items.Add(new Item(lines[0], lines[1], lines[2], int.Parse(lines[3]), double.Parse(lines[4])));
                }
                file.Close();
            }
        }

        public void CalculateSalary()
        {
            foreach (var item in Items)
                foreach (var worker in Workers)
                    if (DidThisWorkerCreateThePart(item.WorkerName, worker.Name, item.WorkerSurname, worker.Surname))
                        worker.setSalary(item.BuiltCount * item.Price + worker.getSalary());
        }

        public bool DidThisWorkerCreateThePart(string WorkerName, string Name, string WorkerSurname, string Surname) => WorkerName == Name && WorkerSurname == Surname;

        public List<Transaction> FormTransactions(string bank)
        {
            List<Transaction> transactions = new List<Transaction>();
            foreach (var worker in Workers)
                if (worker.BankName == bank)
                {
                    try
                    {
                        transactions.Add(new Transaction(worker.BankName, worker.AccountId, worker.getSalary()));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            return transactions;
        }

        public List<Worker> SortBySalaryAndSurname()
        {
            return Workers.OrderBy(x => x.getSalary()).ThenBy(x => x.Surname).ToList();
        }

        public string ConvertItemToCsv()
        {
            string a = "";
            foreach (var item in Items)
                a += item.ToString();
            return a;
        }

        public string ConvertWorkerToCsv()
        {
            string a = "";
            foreach (var item in Workers)
                a += item.ToString();
            return a;
        }
    }
}