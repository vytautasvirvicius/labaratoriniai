﻿using System.IO;
using UnixHate.Models.University.LD1_1;

namespace UnixHate.ViewModels.University.LD1_1
{
    public class LD1_1_ViewModel
    {
        public string filePath { get; set; }
        public string textArea { get; set; }

        public LD1_1_ViewModel()
        {
            filePath = "~/UniversityData/U3.txt";
            textArea = "";
        }

        public Graph CreateGraph()
        {
            var CityCount = 0;
            StreamReader file = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(filePath));
            CityCount = int.Parse(file.ReadLine());
            return null;
        }
    }
}