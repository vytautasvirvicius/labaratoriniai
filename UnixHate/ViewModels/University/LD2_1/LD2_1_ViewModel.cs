﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using UnixHate.Models.University.LD2_1;

namespace UnixHate.ViewModels.University.LD2_1
{
    public class LD2_1_ViewModel
    {
        public string Message = "";
        public string Data;
        public string FilePathModule = "~/UniversityData/U1b.csv";
        public string FilePathStudent = "~/UniversityData/U1a.csv";
        public string Professor;
        public List<Module> ProfessorModuleList = new List<Module>();
        public StudentList Students;
        public ModuleList Modules;

        public LD2_1_ViewModel()
        {
            Students = new StudentList();
            Modules = new ModuleList();
        }

        public string ReturnProfessor()
        {
            return Professor;
        }

        public void WriteStudents()
        {
            for (Students.Begin(); Students.Exists(); Students.Next())
            {
                Console.WriteLine(Students.GetStudent().StudentName);
            }
        }

        public void WriteModuale()
        {
            for (Modules.Begin(); Modules.Exists(); Modules.Next())
            {
                Console.WriteLine(Modules.GetModule().ProfessorName);
            }
        }

        public void ReadStudentData()
        {
            StreamReader file = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(FilePathStudent));
            while (!file.EndOfStream)
            {
                string[] lines = file.ReadLine().Split(';');
                Students.InsertStudent(new Student(lines[0], lines[1], lines[2], lines[3]));
            }
            Message = "Sucessfully read student data.";
            file.Close();
        }

        public void ReadModuleData()
        {
            StreamReader file = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(FilePathModule));
            while (!file.EndOfStream)
            {
                string[] lines = file.ReadLine().Split(';');
                Modules.InsertModule(new Module(lines[0], lines[1], lines[2], int.Parse(lines[3])));
            }
            Message = "Sucessfully read module data.";
           file.Close();
        }

        public string ReturnStudentListCSV()
        {
            string list = "";
            for (Students.Begin(); Students.Exists(); Students.Next())
            {
                Console.WriteLine();
                list +=
                    $"{Students.GetStudent().ModuleName}; {Students.GetStudent().StudentGroup}; {Students.GetStudent().StudentName}; {Students.GetStudent().StudentSurname}; \n";
            }
            return list;
        }

        public string ReturnModuleListCSV()
        {
            string list = "";
            for (Modules.Begin(); Modules.Exists(); Modules.Next())
                list +=
                    $"{Modules.GetModule().ModuleName}; {Modules.GetModule().ProfessorName}; {Modules.GetModule().ProfessorSurname}; {Modules.GetModule().CreditAmount}; \n";
            return list;
        }

        public void RemoveUnpickedModules()
        {
            for (Modules.Begin(); Modules.Exists(); Modules.Next())
            {
                var exists = false;
                for (Students.Begin(); Students.Exists(); Students.Next())
                    if (Modules.GetModule().ModuleName == Students.GetStudent().ModuleName)
                        exists = true;
                if(!exists)
                    Modules.RemoveModule(Modules.GetModule());
            }
        }

        public Module FindMostCommonModule()
        {
            var mostCommon = new Dictionary<Module, int>();
            for (Modules.Begin(); Modules.Exists(); Modules.Next())
            {
                if(!mostCommon.ContainsKey(Modules.GetModule()))
                   mostCommon.Add(Modules.GetModule(), 1);
                mostCommon[Modules.GetModule()]++;
            }
            return mostCommon.OrderByDescending(x => x.Value).ToList()[0].Key;
        }

        public void SortBySurnames()
        {
            Modules.Sort();
        }

        public List<Module> GetModulesByProfessorName(string name, string surname)
        {
            var professorModules = new List<Module>();
            for (Modules.Begin(); Modules.Exists(); Modules.Next())
                if (Modules.GetModule().ProfessorName == name && Modules.GetModule().ProfessorSurname == surname)
                    professorModules.Add(Modules.GetModule());
            return professorModules;
        }

        public string ReturnModuleString(Module item)
        {
            return $"{item.CreditAmount}; {item.ModuleName}; {item.ProfessorName}; {item.ProfessorSurname}";
        }

        public void SendStudentListToDb()
        {

        }
    }
}