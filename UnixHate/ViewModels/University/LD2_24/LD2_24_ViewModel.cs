﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using UnixHate.Models;

namespace UnixHate.ViewModels
{
    public class LD2_24_ViewModel
    {
        public BuyerList Customers = new BuyerList();
        public DeviceList Devices = new DeviceList();

        public DeviceList ReadDeviceData(string fileName)
        {
            DeviceList devices = new DeviceList();
            StreamReader file = new StreamReader(fileName);
            while (!file.EndOfStream)
            {
                string[] lines = file.ReadLine().Split(',');
                devices.InsertDevice(new Device(int.Parse(lines[0]), lines[1], double.Parse(lines[2])));
            }
            file.Close();
            return devices;
        }

        public BuyerList ReadBuyerData(string fileName)
        {
            BuyerList buyers = new BuyerList();
            StreamReader file = new StreamReader(fileName);
            string[] lines = file.ReadLine().Split(',');
            while (!file.EndOfStream)
            {
                buyers.InsertBuyer(new Buyer(lines[0], lines[1], int.Parse(lines[2]), int.Parse(lines[3])));
            }
            file.Close();
            return buyers;
        }

        //Suraskite populiariausią įtaisą, kiek tokių įtaisų parduota ir už kokią sumą
        public string FindMostPopularDevice(BuyerList buyers, DeviceList devices)
        {
            Dictionary<int, int> mostPopularDevice = new Dictionary<int, int>();
            string output = "";
            int soldDevices = 0;
            for(buyers.Begin(); buyers.Exists(); buyers.Next())
            {
                if (mostPopularDevice.ContainsKey(buyers.GetBuyer().DeviceId))
                    mostPopularDevice[buyers.GetBuyer().DeviceId]++;
                mostPopularDevice.Add(buyers.GetBuyer().DeviceId, 1);
            }
            var maxDeviceId = mostPopularDevice.Max().Value;
            for (buyers.Begin(); buyers.Exists(); buyers.Next())
            {
                if (buyers.GetBuyer().DeviceId == maxDeviceId)
                    soldDevices += buyers.GetBuyer().ItemAmount;
            }
            for (devices.Begin(); devices.Exists(); devices.Next())
            {
                if (devices.GetDevice().DeviceId == maxDeviceId)
                    output +=
                        $"DeviceId: {devices.GetDevice().DeviceId} Amount sold: {soldDevices} Revenue: {soldDevices * devices.GetDevice().DevicePrice}\n";
            }
            return output;
        }

        //Sudarykite tik vienos rūšies įtaisus pirkusių pirkėjų sąrašą,
        //nupirktų įtaisų skaičių ir už juos sumokėtų pinigų sumą.
        public string FindMostCommonMerc(BuyerList buyers, DeviceList devices)
        {
            var output = "";
            var repeats = 0;
            var numberBought = 0;
            var revenueGained = 0.00;
            var buyersCopy = buyers;
            var referencedOnce = new BuyerList();
            for (buyers.Begin(); buyers.Exists(); buyers.Next())
            {
                for (buyersCopy.Begin(); buyersCopy.Exists(); buyersCopy.Next())
                    if (buyers.GetBuyer().Surname == buyersCopy.GetBuyer().Surname
                        && buyers.GetBuyer().Name == buyersCopy.GetBuyer().Name)
                        repeats++;
                if(repeats == 1)
                    referencedOnce.InsertBuyer(buyers.GetBuyer());
                repeats = 0;
            }
            for (referencedOnce.Begin(); referencedOnce.Exists(); referencedOnce.Next())
                output += $"Name: {referencedOnce.GetBuyer().Name} Surname: {referencedOnce.GetBuyer().Surname}\n";

            for (referencedOnce.Begin(); referencedOnce.Exists(); referencedOnce.Next())
            {
                numberBought += referencedOnce.GetBuyer().ItemAmount;
                for (devices.Begin(); devices.Exists(); devices.Next())
                    if (referencedOnce.GetBuyer().DeviceId == devices.GetDevice().DeviceId)
                        revenueGained += numberBought * devices.GetDevice().DevicePrice;
            }
            output += $"Item sold: {numberBought} Revenue: {revenueGained}\n";
            return output;
        }

        //Į kitą rinkinį atrinkite įtaisus, kurių parduota ne mažiau kaip n vienetų ir kurių vieneto kaina ne didesnė
        //kaip k litų (n ir k įvedami klaviatūra).
        public DeviceList SelectByProperties(int sold, double price, DeviceList devices, BuyerList buyers)
        {
            var meetsCriteriaForPrice = new DeviceList();
            var meetsCriteriaForSold = new DeviceList();
            var output = new DeviceList();
            for (devices.Begin(); devices.Exists(); devices.Next())
                if (devices.GetDevice().DevicePrice <= price)
                    meetsCriteriaForPrice.InsertDevice(devices.GetDevice());
            for (meetsCriteriaForPrice.Begin(); meetsCriteriaForPrice.Exists(); meetsCriteriaForPrice.Next())
            {
                var soldAmount = 0;
                for (buyers.Begin(); buyers.Exists(); buyers.Next())
                    if (buyers.GetBuyer().DeviceId == meetsCriteriaForPrice.GetDevice().DeviceId)
                        soldAmount += buyers.GetBuyer().ItemAmount;
                if(sold >= soldAmount)
                    meetsCriteriaForSold.InsertDevice(meetsCriteriaForPrice.GetDevice());
            }
            return meetsCriteriaForSold;
        }
    }
}