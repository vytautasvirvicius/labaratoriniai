﻿using System.Web.Mvc;

namespace UnixHate.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexAfterLogin()
        {
            return View();
        }


        [HttpPost]
        public ActionResult First()
        {
            return RedirectToAction("LD1_1_Index", "University");
        }

        [HttpPost]
        public ActionResult Second_24()
        {
            return RedirectToAction("LD2_24_Index", "University");
        }

        [HttpPost]
        public ActionResult Third()
        {
            return RedirectToAction("LD3_1_Index", "University");
        }

        public ActionResult Second_1()
        {
            return RedirectToAction("LD2_1_Index", "University");
        }

        public ActionResult Fourth()
        {
            return RedirectToAction("LD4_1_Index", "University");
        }
    }
}