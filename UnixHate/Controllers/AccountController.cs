﻿using System.Web.Mvc;
using UnixHate.Models;
using UnixHate.ViewModels;

//TODO: Create authentication for user so he cannot access stuff he shouldn't

namespace UnixHate.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            var model = new SiteUser_ViewModel();
            return View(model);
        }

        public ActionResult Register()
        {
            var model = new SiteUser_ViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(SiteUser_ViewModel model)
        {
            var username = model.Customer.AccountName;
            var password = model.Customer.Password;
            var database = new DatabaseUser();
            model.Message = database.LoginUser(username, password);
            return RedirectToAction("IndexAfterLogin", "Home");
        }

        [HttpPost]
        public ActionResult Register(SiteUser_ViewModel model)
        {
            var username = model.Customer.AccountName;
            if (model.Customer.Password != model.Customer.RepeatedPassword)
            {
                model.Message = "Passwords do not match";
                return View("Register", model);
            }
            var salt = model.Encrypt.CreateSalt(8);
            var saltedHash = model.Encrypt.GenerateSHA256Hash(model.Customer.Password, salt);
            var database = new DatabaseUser();
            if (database.CheckForDuplicateUser(model.Customer.AccountName))
            {
                model.Message = database.RegisterUser(username, saltedHash, salt);
                return View("Register", model);
            }
            model.Message = "Username is already in use.";
            return View("Register", model);
        }
    }
}