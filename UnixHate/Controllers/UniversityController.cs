﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using StructureMap.Pipeline;
using StructureMap.Query;
using UnixHate.Models;
using UnixHate.Models.University.LD2_1;
using UnixHate.Models.University.LD4_1;
using UnixHate.ViewModels.University.LD1_1;
using UnixHate.ViewModels.University.LD2_1;
using UnixHate.ViewModels.University.LD3_1;
using UnixHate.ViewModels.University.LD4_1;

namespace UnixHate.Controllers
{
    public class UniversityController : Controller
    {

        private readonly LD2_1_ViewModel _model = new LD2_1_ViewModel();
        private readonly DatabaseLD2_1 _dbData = new DatabaseLD2_1();
        private readonly LD3_1_ViewModel _model2 = new LD3_1_ViewModel();
        private readonly DatabaseLD3_1 _dbData2 = new DatabaseLD3_1();
        private readonly LD4_1_ViewModel _model3 = new LD4_1_ViewModel();
        private readonly DatabaseLD4_1 _dbData3 = new DatabaseLD4_1();

        public ActionResult LD1_1_Index()
        {
            var model = new LD1_1_ViewModel();
            return View(model);
        }

        public ActionResult LD2_24_Index()
        {
            return View();
        }

        public ActionResult LD3_1_Index()
        {
            var model = new LD3_1_ViewModel();
            return View(model);
        }

        public ActionResult LD2_1_Index()
        {
            var model = new LD2_1_ViewModel();
            return View(model);
        }

        public ActionResult LD4_1_Index()
        {
            var model = new LD4_1_ViewModel();
            return View(model);
        }

        // 2

        [HttpPost]
        public ActionResult GetModuleData()
        {
            if(!string.IsNullOrEmpty(_model.FilePathModule))
                _model.ReadModuleData();
            _model.Data = _model.ReturnModuleListCSV();
            _dbData.SendModulesListToDb(_model.Modules);
            return View("LD2_1_Index", _model);
        }

        [HttpPost]
        public ActionResult GetStudentData()
        {
            if(!string.IsNullOrEmpty(_model.FilePathStudent))
                _model.ReadStudentData();
            _model.Data = _model.ReturnStudentListCSV();
            _dbData.SendStudentListToDb(_model.Students);
            return View("LD2_1_Index", _model);
        }

        [HttpPost]
        public ActionResult FindProfessorModules(string professor)
        {
            _model.Modules = _dbData.GetModulesFromDb();
            _model.Professor = professor;
            _model.ProfessorModuleList = new List<Module>();
            _model.ProfessorModuleList =
                _model.GetModulesByProfessorName(_model.Professor.Split(' ')[0], _model.Professor.Split(' ')[1]);
            return View("LD2_1_Index", _model);
        }

        [HttpPost]
        public ActionResult FormProfessorList()
        {
            _model.Modules = _dbData.GetModulesFromDb();
            _model.Data = _model.ReturnModuleListCSV();
            return View("LD2_1_Index", _model);
        }

        [HttpPost]
        public ActionResult RemoveUnpickedProfessors()
        {
            _model.Students = _dbData.GetStudentsFromDb();
            _model.Modules = _dbData.GetModulesFromDb();
            _model.RemoveUnpickedModules();
            _model.Data = _model.ReturnModuleListCSV();
            return View("LD2_1_Index", _model);
        }

        [HttpPost]
        public ActionResult FindMostCommonProfessor()
        {
            _model.Modules = _dbData.GetModulesFromDb();
            _model.Data = _model.FindMostCommonModule().ToString();
            return View("LD2_1_Index", _model);
        }

        // 3

        [HttpPost]
        public ActionResult GetModuleData2()
        {
            if(!string.IsNullOrEmpty(_model2.FilePathModule))
                _model2.ReadModuleData();
            _model2.Data = _model2.ReturnModuleListCSV();
            _dbData2.SendModulesListToDb(_model2.Modules);
            return View("LD3_1_Index", _model2);
        }

        [HttpPost]
        public ActionResult GetStudentData2()
        {
            if(!string.IsNullOrEmpty(_model2.FilePathStudent))
                _model2.ReadStudentData();
            _model2.Data = _model2.ReturnStudentListCSV();
            _dbData2.SendStudentListToDb(_model2.Students);
            return View("LD3_1_Index", _model2);
        }

        [HttpPost]
        public ActionResult FindProfessorModules2(string professor)
        {
            _model2.Modules = _dbData2.GetModulesFromDb();
            _model2.Professor = professor;
            _model2.ProfessorModuleList = new List<Module>();
            _model2.ProfessorModuleList =
                _model2.GetModulesByProfessorName(_model2.Professor.Split(' ')[0], _model2.Professor.Split(' ')[1]);
            return View("LD3_1_Index", _model2);
        }

        [HttpPost]
        public ActionResult FormProfessorList2()
        {
            _model2.Modules = _dbData2.GetModulesFromDb();
            _model2.Data = _model2.ReturnModuleListCSV();
            return View("LD3_1_Index", _model2);
        }

        [HttpPost]
        public ActionResult RemoveUnpickedProfessors2()
        {
            _model2.Students = _dbData2.GetStudentsFromDb();
            _model2.Modules = _dbData2.GetModulesFromDb();
            _model2.RemoveUnpickedModules();
            _model2.Data = _model2.ReturnModuleListCSV();
            return View("LD3_1_Index", _model2);
        }

        [HttpPost]
        public ActionResult FindMostCommonProfessor2()
        {
            _model2.Modules = _dbData2.GetModulesFromDb();
            _model2.Data = _model2.FindMostCommonModule().ToString();
            return View("LD3_1_Index", _model2);
        }

        // 4/5

        public ActionResult GetItemData()
        {
            _dbData3.DeleteItemData();
            _model3.ReadItemData();
            _model3.Data = _model3.ConvertItemToCsv();
            _dbData3.SendItemListToDb(_model3.Items);
            return View("LD4_1_Index", _model3);

        }

        public ActionResult GetWorkerData()
        {
            _dbData3.DeleteWorkerData();
            _model3.ReadWorkerData();
            _model3.Data = _model3.ConvertWorkerToCsv();
            _dbData3.SendWorkerListToDb(_model3.Workers);
            return View("LD4_1_Index", _model3);
        }

        public ActionResult FindBankTransactions(string bank)
        {
            _model3.Items = _dbData3.GetItemsFromDb();
            _model3.Workers = _dbData3.GetWorkersFromDb();
            _model3.Bank = bank;
            _model3.Transactions = _model3.FormTransactions(bank);
            return View("LD4_1_Index", _model3);
        }

        public ActionResult DeleteDataFromDb()
        {
            _dbData3.DeleteItemData();
            _dbData3.DeleteTransactionData();
            _dbData3.DeleteWorkerData();
            return View("LD4_1_Index", _model3);
        }

        public ActionResult FindSalaries()
        {
            _model3.Items = _dbData3.GetItemsFromDb();
            _model3.Workers = _dbData3.GetWorkersFromDb();
            _model3.CalculateSalary();
            return View("LD4_1_Index", _model3);
        }
    }
}