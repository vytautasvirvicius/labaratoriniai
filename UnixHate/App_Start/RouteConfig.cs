﻿using System.Web.Mvc;
using System.Web.Routing;

namespace UnixHate
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );
            routes.MapRoute(
                "GetProfessor",
                "{controller}/{action}/{id}",
                new {controller = "University", action = "FindProfessorModules", id = UrlParameter.Optional}
            );
        }
    }
}