﻿using System;
using System.Runtime.ConstrainedExecution;
using MySql.Data.MySqlClient;
using UnixHate.Models.University.LD2_1;


//TO DO: Make method to check if username is unique. Or create a throw exception that doesn't link user to error page

namespace UnixHate.Models
{
    public class DatabaseUser
    {
        private readonly MySqlConnection _connection = new MySqlConnection(System.Configuration.ConfigurationManager
            .ConnectionStrings["user_information"]
            .ConnectionString);

        private readonly DatabaseErrors _errors = new DatabaseErrors();

        public string RegisterUser(string username, string saltedPassword, string salt)
        {
            _connection.Open();
            InsertUserIntoDatabase(username, saltedPassword, salt);
            _connection.Close();
            //TO DO: Run authentication and redirect to page after log-in
            return "User succesfully registered";
        }

        public string LoginUser(string username, string password)
        {
            _connection.Open();
            string saltedPassword;
            string salt;
            GetUserPasswordFromDatabase(username, out saltedPassword, out salt);
            _connection.Close();
            if (saltedPassword != new Encrypt().GenerateSHA256Hash(password, salt))
                return _errors.PasswordIsIncorrect;
            //TO DO: Authorize user to use site freely
            return "User succesfully logged in";
        }

        public void InsertUserIntoDatabase(string username, string saltedPassword, string salt)
        {
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText =
                "INSERT INTO user_information (Username, Password, Salt) VALUES (@username, @password, @salt)";
            command.Parameters.AddWithValue("@username", username);
            command.Parameters.AddWithValue("@password", saltedPassword);
            command.Parameters.AddWithValue("@salt", salt);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void GetUserPasswordFromDatabase(string username, out string saltedPassword, out string salt)
        {
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM user_information WHERE Username = @username";
            command.Parameters.AddWithValue("@username", username);
            try
            {
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                saltedPassword = reader.GetString("Password");
                salt = reader.GetString("Salt");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool CheckForDuplicateUser(string username)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT Username FROM user_information WHERE Username=@username";
            command.Parameters.AddWithValue("@username", username);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
                if (reader.GetString(0) == username)
                {
                    _connection.Close();
                    return false;
                }
            _connection.Close();
            return true;
        }
    }
}
