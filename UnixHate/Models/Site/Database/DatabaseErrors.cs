﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace UnixHate.Models
{
    public class DatabaseErrors
    {
        public readonly string DatabaseConnectionNotOpen = "Database connection is not open.";
        public readonly string UserIsNotUnique = "Username already exists.";
        public readonly string UserDoesNotExist = "User does not exist.";
        public readonly string PasswordIsIncorrect = "Password is incorrect.";
    }
}