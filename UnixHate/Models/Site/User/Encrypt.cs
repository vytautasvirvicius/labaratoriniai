﻿using System;
using System.Security.Cryptography;

namespace UnixHate.Models
{
    public class Encrypt
    {
        public String CreateSalt(int size)
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }

        public String GenerateSHA256Hash(String input, String salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input + salt);
            SHA256Managed sha256Hashstring = new SHA256Managed();
            byte[] hash = sha256Hashstring.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }
    }
}