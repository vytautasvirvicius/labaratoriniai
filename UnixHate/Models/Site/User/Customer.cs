﻿using System.ComponentModel.DataAnnotations;

namespace UnixHate.Models
{
    public class Customer
    {
        public string AccountName { get; set; }
        public string Password { get; set; }
        public string RepeatedPassword { get; set; }
    }
}
