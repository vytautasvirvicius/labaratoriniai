﻿namespace UnixHate.Models
{
    public class DeviceList
    {
        private class Node
        {
            public Device Data { get; set; }
            public Node Next { get; set; }

            public Node(Device data, Node next)
            {
                Data = data;
                Next = next;
            }
        }

        private Node _pr;
        private Node _pb;
        private Node _d;

        public DeviceList()
        {
            _pr = null;
            _pb = null;
            _d = null;
        }

        public void Begin()
        {
            _d = _pr;
        }

        public void Next()
        {
            _d = _d.Next;
        }

        public bool Exists()
        {
            return _d != null;
        }

        public Device GetDevice()
        {
            return _d.Data;
        }

        public void InsertDevice(Device item)
        {
            if (_pr == null)
            {
                _pr = new Node(item, null);
                _pb = _pr;
            }
            else
            {
                _pb.Next = new Node(item, null);
                _pb = _pb.Next;
            }
        }
    }
}