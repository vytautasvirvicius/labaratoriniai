﻿namespace UnixHate.Models
{
    public class Device
    {
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public double DevicePrice { get; set; }

        //Constructor
        public Device(int deviceId, string deviceName, double devicePrice)
        {
            DeviceId = deviceId;
            DeviceName = deviceName;
            DevicePrice = devicePrice;
        }
    }
}