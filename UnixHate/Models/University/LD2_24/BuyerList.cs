﻿namespace UnixHate.Models
{
    public class BuyerList
    {
        private sealed class Node
        {
            public Buyer Data { get; set; }
            public Node Next { get; set; }

            public Node(Buyer data, Node next)
            {
                Data = data;
                Next = next;
            }
        }

        private Node _pr;
        private Node _pb;
        private Node _d;

        public BuyerList()
        {
            _pr = null;
            _pb = null;
            _d = null;
        }

        public void Begin()
        {
            _d = _pr;
        }

        public void Next()
        {
            _d = _d.Next;
        }

        public bool Exists()
        {
            return _d != null;
        }

        public Buyer GetBuyer()
        {
            return _d.Data;
        }

        public void InsertBuyer(Buyer item)
        {
            if (_pr == null)
            {
                _pr = new Node(item, null);
                _pb = _pr;
            }
            else
            {
                _pb.Next = new Node(item, null);
                _pb = _pb.Next;
            }
        }
    }
}
