﻿namespace UnixHate.Models
{
    public class Buyer
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public int DeviceId { get; set; }
        public int ItemAmount { get; set; }

        //Constructor
        public Buyer(string surname, string name, int deviceId, int itemAmount)
        {
            Surname = surname;
            Name = name;
            DeviceId = deviceId;
            ItemAmount = itemAmount;
        }
    }
}