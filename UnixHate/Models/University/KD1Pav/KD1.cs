﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UnixHate.Models.University.KD1Pav
{
    public class Kd1
    {
        static Faculty Readfile(string fileName)
        {
            StreamReader file = new StreamReader(fileName);
            string[] lines = file.ReadLine().Split(',');
            var faculty = new Faculty(lines[0], int.Parse(lines[1]), int.Parse(lines[2]), new List<Student>());
            while (!file.EndOfStream)
            {
                List<string> information = file.ReadLine().Split(',').ToList();
                List<int> credits = new List<int>();
                for (var i = 0; i < information.Count; i++)
                    if (i > 2)
                        credits.Add(int.Parse(information[i]));
                faculty.Students.Add(new Student(information[0], information[1], information[2], credits));
            }
            file.Close();
            return faculty;
        }

        static Faculty WriteToFile()
        {
            return null;
        }
    }

    public class Faculty
    {
        public string FacultyName { get; set; }
        public int Credits { get; set; }
        public int Modules { get; set; }
        public List<Student> Students { get; set; }

        public Faculty(string facultyName, int credits, int modules, List<Student> students)
        {
            FacultyName = facultyName;
            Credits = credits;
            Modules = modules;
            Students = students;
        }

        public List<Student> StudentsThatExceededCredits()
        {
            return Students.Where(item => item.SumCredits(item.ModulesCreditList.Count) > Credits).ToList();
        }

        public List<Student> SortByGroupAndName()
        {
            return Students.OrderBy(item => item.Group).ThenBy(item => item.Surname).ToList();
        }

        public static bool operator >(Faculty lhs, Faculty rhs)
        {
            return lhs.Students.Count > rhs.Students.Count;
        }

        public static bool operator <(Faculty lhs, Faculty rhs)
        {
            return !(lhs > rhs);
        }

        public static bool operator ==(Faculty lhs, Faculty rhs)
        {
            return lhs.Students.Count == rhs.Students.Count;
        }

        public static bool operator !=(Faculty lhs, Faculty rhs)
        {
            return !(lhs == rhs);
        }

        protected bool Equals(Faculty other)
        {
            return string.Equals(FacultyName, other.FacultyName) && Credits == other.Credits && Modules == other.Modules && Equals(Students, other.Students);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Faculty) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (FacultyName != null ? FacultyName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Credits;
                hashCode = (hashCode * 397) ^ Modules;
                hashCode = (hashCode * 397) ^ (Students != null ? Students.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    public class Student
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
        public List<int> ModulesCreditList { get; set; }

        public Student(string surname, string name, string group, List<int> modulesCreditList)
        {
            Surname = surname;
            Name = name;
            Group = group;
            ModulesCreditList = modulesCreditList;
        }

        public int SumCredits(int n)
        {
            if (n < 0)
                return 0;
            return ModulesCreditList[n] + SumCredits(n - 1);
        }
    }
}