﻿namespace UnixHate.Models.University.LD2_1
{
    public class Module
    {
        public string ModuleName { get; set; }
        public string ProfessorSurname { get; set; }
        public string ProfessorName { get; set; }
        public int CreditAmount { get; set; }

        public Module(string moduleName, string professorSurname, string professorName, int creditAmount)
        {
            ModuleName = moduleName;
            ProfessorSurname = professorSurname;
            ProfessorName = professorName;
            CreditAmount = creditAmount;
        }

        public override string ToString()
        {
            return $"{ModuleName};{ProfessorSurname};{ProfessorName};{CreditAmount};";
        }
    }
}