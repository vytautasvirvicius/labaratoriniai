﻿using System.Data;
using MySql.Data.MySqlClient;
using UnixHate.Models.University.LD2_1;

namespace UnixHate.Models
{
    public class DatabaseLD2_1
    {
        private readonly MySqlConnection _connection = new MySqlConnection(System.Configuration.ConfigurationManager
            .ConnectionStrings["user_information"]
            .ConnectionString);
        private readonly DatabaseErrors _errors = new DatabaseErrors();

        public void AddStudentToDb(Student student)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "INSERT INTO Students (Student) VALUES (@student)";
            command.Parameters.AddWithValue("@student", student.ToString());
            command.ExecuteNonQuery();
            _connection.Close();
        }

        public void AddModuleToDb(Module module)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "INSERT INTO Modules (Modules) VALUES (@module)";
            command.Parameters.AddWithValue("@module", module.ToString());
            command.ExecuteNonQuery();
            _connection.Close();
        }

        public void SendStudentListToDb(StudentList students)
        {
            _connection.Open();
            MySqlCommand truncate = _connection.CreateCommand();
            truncate.CommandText = "TRUNCATE TABLE Students";
            truncate.ExecuteNonQuery();
            _connection.Close();
            for (students.Begin(); students.Exists(); students.Next())
                AddStudentToDb(students.GetStudent());
        }

        public void SendModulesListToDb(ModuleList modules)
        {
            _connection.Open();
            MySqlCommand truncate = _connection.CreateCommand();
            truncate.CommandText = "TRUNCATE TABLE Modules";
            truncate.ExecuteNonQuery();
            _connection.Close();
            for (modules.Begin(); modules.Exists(); modules.Next())
                AddModuleToDb(modules.GetModule());
        }

        public ModuleList GetModulesFromDb()
        {
            ModuleList modules = new ModuleList();
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM Modules";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string[] lines = reader.GetString("Modules").Split(';');
                modules.InsertModule(new Module(lines[0], lines[1], lines[2], int.Parse(lines[3])));
            }
            _connection.Close();
            return modules;
        }

        public StudentList GetStudentsFromDb()
        {
            StudentList students = new StudentList();
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM Students";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string[] lines = reader.GetString("Student").Split(';');
                students.InsertStudent(new Student(lines[0], lines[1], lines[2], lines[3]));
            }
            _connection.Close();
            return students;
        }
    }
}