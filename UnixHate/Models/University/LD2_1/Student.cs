﻿namespace UnixHate.Models.University.LD2_1
{
    public class Student
    {
        public string ModuleName { get; set; }
        public string StudentSurname { get; set; }
        public string StudentName { get; set; }
        public string StudentGroup { get; set; }

        public Student(string moduleName, string studentSurname, string studentName, string studentGroup)
        {
            ModuleName = moduleName;
            StudentSurname = studentSurname;
            StudentName = studentName;
            StudentGroup = studentGroup;
        }

        public override string ToString()
        {
            return $"{ModuleName};{StudentSurname};{StudentName};{StudentGroup};";
        }
    }
}