﻿namespace UnixHate.Models.University.LD2_1
{
    public class StudentList
    {
        private sealed class Node
        {
            public Student Data { get; set; }
            public Node Next { get; set; }
            public Node Previous { get; set; }

            public Node(Student data, Node next, Node previous)
            {
                Data = data;
                Next = next;
                Previous = previous;
            }
        }

        private Node _pr;
        private Node _pb;
        private Node _d;

        public StudentList()
        {
            _pr = null;
            _pb = null;
            _d = null;
        }

        public void Begin()
        {
            _d = _pr;
        }

        public void Next()
        {
            _d = _d.Next;
        }

        public bool Exists()
        {
            return _d != null;
        }

        public Student GetStudent()
        {
            return _d.Data;
        }

        public void InsertStudent(Student item)
        {
            if (_pr == null)
            {
                _pr = new Node(item, null, null);
                _pb = _pr;
            }
            else
            {
                _pb.Next = new Node(item, null, _pb);
                _pb = _pb.Next;
            }
        }

        public void RemoveStudent(Student item)
        {
            var place = new Node(null, null, null);
            for (Begin(); Exists(); Next())
                if (item == GetStudent())
                    place = _d;
            for (Begin(); Exists(); Next())
                if (_d.Next == place)
                {
                    if (_d.Next != null)
                        _d = _d.Next.Next;
                    _d.Next = null;
                }
        }
    }
}