﻿using System;

namespace UnixHate.Models.University.LD2_1
{
    public class ModuleList
    {
        private sealed class Node
        {
            public Module Data { get; set; }
            public Node Next { get; set; }
            public Node Previous { get; set; }

            public Node(Module data, Node next, Node previous)
            {
                Data = data;
                Next = next;
                Previous = previous;
            }
        }

        private Node _pr;
        private Node _pb;
        private Node _d;

        public ModuleList()
        {
            _pr = null;
            _pb = null;
            _d = null;
        }

        public void Begin()
        {
            _d = _pr;
        }

        public void Next()
        {
            _d = _d.Next;
        }

        public bool Exists()
        {
            return _d != null;
        }

        public Module GetModule()
        {
            return _d.Data;
        }

        public void InsertModule(Module item)
        {
            if (_pr == null)
            {
                _pr = new Node(item, null, null);
                _pb = _pr;
            }
            else
            {
                _pb.Next = new Node(item, null, _pb);
                _pb = _pb.Next;
            }
        }

        public void RemoveModule(Module item)
        {
            for (Begin(); Exists(); Next())
                if (item.ModuleName == GetModule().ModuleName)
                {
                    _pr.Next = _d.Next;
                    _d = null;
                }
        }

        public void Sort ()
        {
            for (var i = _pr; i != null; i = i.Next)
            {
                var max = i;
                for (var j = i.Next; j != null; j = j.Next)
                    if (string.CompareOrdinal(max.Data.ProfessorSurname, j.Data.ProfessorSurname) < 0)
                        max = j;
                var temp = i.Data;
                i.Data = max.Data;
                max.Data = temp;
            }
        }
    }
}