﻿using System.Data;
using MySql.Data.MySqlClient;
using UnixHate.Models.University.LD2_1;

namespace UnixHate.Models
{
    public class DatabaseLD3_1
    {
        private readonly MySqlConnection _connection = new MySqlConnection(System.Configuration.ConfigurationManager
            .ConnectionStrings["user_information"]
            .ConnectionString);
        private readonly DatabaseErrors _errors = new DatabaseErrors();

        public void AddStudentToDb(Student student)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "INSERT INTO Students (Student) VALUES (@student)";
            command.Parameters.AddWithValue("@student", student.ToString());
            command.ExecuteNonQuery();
            _connection.Close();
        }

        public void AddModuleToDb(Module module)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "INSERT INTO Modules (Modules) VALUES (@module)";
            command.Parameters.AddWithValue("@module", module.ToString());
            command.ExecuteNonQuery();
            _connection.Close();
        }

        public void SendStudentListToDb(LinkedList<Student> students)
        {
            _connection.Open();
            MySqlCommand truncate = _connection.CreateCommand();
            truncate.CommandText = "TRUNCATE TABLE Students";
            truncate.ExecuteNonQuery();
            _connection.Close();
            foreach(var item in students)
                AddStudentToDb(item);

        }

        public void SendModulesListToDb(LinkedList<Module> modules)
        {
            _connection.Open();
            MySqlCommand truncate = _connection.CreateCommand();
            truncate.CommandText = "TRUNCATE TABLE Modules";
            truncate.ExecuteNonQuery();
            _connection.Close();
            foreach(var item in modules)
                AddModuleToDb(item);
        }

        public LinkedList<Module> GetModulesFromDb()
        {
            LinkedList<Module> modules = new LinkedList<Module>();
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM Modules";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string[] lines = reader.GetString("Modules").Split(';');
                modules.Add(new Module(lines[0], lines[1], lines[2], int.Parse(lines[3])));
            }
            _connection.Close();
            return modules;
        }

        public LinkedList<Student> GetStudentsFromDb()
        {
            LinkedList<Student> students = new LinkedList<Student>();
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM Students";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string[] lines = reader.GetString("Student").Split(';');
                students.Add(new Student(lines[0], lines[1], lines[2], lines[3]));
            }
            _connection.Close();
            return students;
        }
    }
}