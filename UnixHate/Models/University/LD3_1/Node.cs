﻿/// <summary>
/// Summary description for Node
/// </summary>
public class Node<T>
{
    public T Data { set; get; }
    public Node<T> Next { set; get; }
    public Node<T> Next2 { set; get; }

    public Node(T data)
    {
        Data = data;
        Next = null;
    }

    public Node()
    {
        Data = default(T);
        Next = null;
    }

    public bool Equals(T data)
    {
        return Data.Equals(data);
    }
}