﻿using System;
using System.Collections;
using System.Collections.Generic;

public class LinkedList<T> : ICollection<T>, IEnumerable<T>, IEnumerable
{
    public Node<T> First { private set; get; }
    public Node<T> Last { private set; get; }
    public int Count { private set; get; }

    public bool IsReadOnly
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public LinkedList()
    {
        First = null;
        Last = null;
        Count = 0;
    }

    public void Add(T item)
    {
        Count++;
        Node<T> tmp = new Node<T>(item);
        if (First == null)
        {
            First = Last = tmp;
            return;
        }

        Last.Next = tmp;
        Last = tmp;
    }

    public bool Remove(T item)
    {
        if (First.Equals(item))
        {
            RemoveFirst();
            return true;
        }

        Node<T> tmp = First;
        while (tmp.Next != null)
        {
            if (tmp.Next.Equals(item))
            {
                tmp.Next = tmp.Next.Next;
                Count--;
                return true;
            }
            tmp = tmp.Next;
        }

        return false;
    }

    public void RemoveFirst()
    {
        if (Count == 0)
            throw new InvalidOperationException("LinkedList is empty");
        First = First.Next;
        Count--;
    }

    public void Clear()
    {
        First = null;
        Last = null;
        Count = 0;
    }

    public bool Contains(T item)
    {
        Node<T> tmp = First;

        while(tmp != null)
        {
            if (First.Equals(item))
                return true;
            tmp = tmp.Next;
        }

        return false;
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
        if (array == null)
            throw new ArgumentNullException();

        if (arrayIndex < 0)
            throw new IndexOutOfRangeException();

        if (array.Length - arrayIndex < Count)
            throw new ArgumentException("Array is smaller than number of elements in LinkedList");

        Node<T> tmp = First;
        while(tmp != null)
        {
            array[arrayIndex++] = tmp.Data;
            tmp = tmp.Next;
        }
    }

    public Node<T> Find(T item)
    {
        Node<T> tmp = First;
        while (tmp != null)
            if (item.Equals(tmp.Data))
                return tmp;
            else tmp = tmp.Next;
        return null;
    }

    public IEnumerator<T> GetEnumerator()
    {
        return new LinkedListEnumerator<T>(First);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return new LinkedListEnumerator<T>(First) as IEnumerator;
    }


    private class LinkedListEnumerator<T> : IEnumerator<T>
    {
        private Node<T> First { set; get; }
        private Node<T> Position { set; get; }

        public LinkedListEnumerator(Node<T> first)
        {
            First = new Node<T>();
            First.Next = first;
            Position = First;
        }

        public T Current => Position.Data;
        object IEnumerator.Current => Position.Data;

        public void Dispose()
        {
            First = null;
            Position = null;
        }

        public bool MoveNext()
        {
            if(Position.Next != null)
            {
                Position = Position.Next;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            Position = First;
        }
    }
}