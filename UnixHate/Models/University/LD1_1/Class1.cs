using System;
using System.IO;

public class Map
{
    public const int MaxNodeCount = 81;

    protected void Button1_Click1(object sender, EventArgs e)
    {
        const string fd = @"App_Data\\D.txt";
        const string fr = @"App_data\\R.txt";
        int n;
        KeliuKonteineris Keliai = new KeliuKonteineris(MaxNodeCount);
        Skaitymas(fd, out n, Keliai);
        IntKonteineris Miestai = new IntKonteineris(n + 1);
        int m = 1;
        int c = 0;
        int et = 1;
        SudarytiTaka(n, Keliai, ref m, Miestai, ref c, ref et);
        if (c == n && m == 1)
        {
            Patikrinimas(Keliai, n, ref et);
        }
        Spausdinti(Keliai, n, fr, ref c, ref m);
    }

    /// <summary>
    /// Skaitymo iš failo metodas
    /// </summary>
    /// <param name="fd">Duomenų failo nuoroda</param>
    /// <param name="n">Miestų skaičius</param>
    /// <param name="Keliai">Kelių konteineris</param>
    void Skaitymas(string fd, out int n, KeliuKonteineris Keliai)
    {
        n = 0;
        using (StreamReader s = new StreamReader(Server.MapPath(fd)))
        {
            n = int.Parse(s.ReadLine());
            string line = null;
            while ((line = s.ReadLine()) != null)
            {
                string[] values = line.Split(' ');
                int kelias1 = int.Parse(values[0]);
                int kelias2 = int.Parse(values[1]);
                Kelias k = new Kelias(kelias1, kelias2, false, 0);
                Keliai.PridetiKelia(k);
                Kelias b = new Kelias(kelias2, kelias1, false, 0);
                Keliai.PridetiKelia(b);
            }
        }
    }

    static int Miestas(int sk)
    {
        double lol = sk;
        int yes = (int)Math.Ceiling(lol / 4);
        return yes;
    }

    /// <summary>
    /// Rekursinis metodas sudarantis taką tarp visų miestų, nuo sienos iki sienos
    /// </summary>
    /// <param name="n">Miestų skaičius</param>
    /// <param name="Keliai">Kelių konteineris</param>
    /// <param name="m">Kurjerio dabartinė vieta (miestas)</param>
    /// <param name="Miestai">Int konteineris</param>
    /// <param name="c">Skaičius, matuojantis kiek miestų aplankė kurjeris</param>
    /// <param name="et">Skaičius eilės tvarkai nustatyti</param>
    static void SudarytiTaka(int n, KeliuKonteineris Keliai, ref int m, IntKonteineris Miestai, ref int c, ref int et)
    {
        int k = 0;
        for (int i = 1; i < Keliai.Skaicius; i++)
        {
            Miestai.NustatytiSkaiciu(m, 1);
            if (m == Miestas(Keliai.GautiKelia(i).Kelias1) && !Keliai.GautiKelia(i).ArPazymejo)
            {
                k = 1;
                m = Miestas(Keliai.GautiKelia(i).Kelias2);
                Keliai.GautiKelia(i).ArPazymejo = true;

                for (int j = 1; j < Keliai.Skaicius; j++)
                {
                    if (Keliai.GautiKelia(j).Kelias1 == Keliai.GautiKelia(i).Kelias2)
                    {
                        Keliai.GautiKelia(j).ArPazymejo = true;
                        Keliai.GautiKelia(i).EilesTvarka = et++;
                        Keliai.GautiKelia(j).EilesTvarka = et++;
                    }
                }
            }
        }

        if (k == 0)
        {
            for (int i = 1; i <= n; i++)
            {
                if (Miestai.GautiSkaiciu(i) == 1)
                    c++;
            }
        }
        else
        {
            SudarytiTaka(n, Keliai, ref m, Miestai, ref c, ref et);
        }
    }

    /// <summary>
    /// Funkcija rezultatų spausdinimui į failą.
    /// </summary>
    /// <param name="Keliai">Kelių konteineris</param>
    /// <param name="n">Miestų skaičius</param>
    /// <param name="fr">Rezultatų failo nuoroda</param>
    /// <param name="c">Int skaičiuojantis kiek miestų aplankė kurjeris</param>
    /// <param name="m">Dabartinė kurjerio vieta (miestas)</param>
    void Spausdinti(KeliuKonteineris Keliai, int n, string fr, ref int c, ref int m)
    {
        int k = 1;
        using (StreamWriter r = new StreamWriter(Server.MapPath(fr)))
        {
            if (c == n && m == 1)
            {
                r.WriteLine("Taip");
                ResultLabel.Text = "Taip" + "<br />";
                for (int i = 1; i < Keliai.Skaicius; i++)
                {
                    if (k == Keliai.GautiKelia(i).EilesTvarka)
                    {
                        ResultLabel.Text = ResultLabel.Text + Keliai.GautiKelia(i).Kelias1 + " ";
                        r.Write(Keliai.GautiKelia(i).Kelias1 + " ");
                        i = 1;
                        k++;
                    }
                }
            }
            else
            {
                r.WriteLine("Ne");
                ResultLabel.Text = "Ne" + "<br />";
            }
        }
    }

    /// <summary>
    /// Metodas, patikrinantis ar yra nepažymėtų sienų, ir jei tai yra įmanoma, jas sužymi
    /// </summary>
    /// <param name="Keliai">Kelių konteineris</param>
    /// <param name="n">Miestų skaičius</param>
    /// <param name="et">Eilės tvarka</param>
    static void Patikrinimas(KeliuKonteineris Keliai, int n, ref int et)
    {
        int m;
        int konst;

        for (int i = 1; i < Keliai.Skaicius; i++)
        {
            if (!Keliai.GautiKelia(i).ArPazymejo)
            {
                m = Miestas(Keliai.GautiKelia(i).Kelias1);
                for (int j = 1; j < Keliai.Skaicius; j++)
                {
                    if (m == Miestas(Keliai.GautiKelia(j).Kelias1) && i != j)
                    {
                        konst = Keliai.GautiKelia(j).EilesTvarka + 1;
                        for (int k = 1; k < Keliai.Skaicius; k++)
                        {
                            if (Keliai.GautiKelia(k).EilesTvarka >= konst)
                            {
                                Keliai.GautiKelia(k).EilesTvarka++;
                            }
                        }
                        Keliai.GautiKelia(i).EilesTvarka = konst;
                        break;
                    }
                }
                Keliai.GautiKelia(i).ArPazymejo = true;
            }
        }
    }
}

/// <summary>
/// Int reikšmių klasė
/// </summary>
class IntKonteineris
{
    public int[] Skaiciai { get; set; }
    public int Skaicius { get; private set; }

    /// <summary>
    /// Konstruktorius
    /// </summary>
    /// <param name="dydis">Konteinerio dydis</param>
    public IntKonteineris(int dydis)
    {
        Skaiciai = new int[dydis];
    }

    /// <summary>
    /// Metodas pridedantis skaičių į konteinerį
    /// </summary>
    public void PridetiSkaiciu(int sk)
    {
        Skaiciai[Skaicius++] = sk;
    }

    /// <summary>
    /// Metodas leidžiantis paimti skaičių iš konteinerio
    /// </summary>
    public int GautiSkaiciu(int index)
    {
        return Skaiciai[index];
    }

    public void NustatytiSkaiciu(int index, int sk)
    {
        Skaiciai[index] = sk;
    }

}

    class Kelias
{
    
    public int Kelias1 { get; set; }
    public int Kelias2 { get; set; }
    public bool ArPazymejo { get; set; }
    public int EilesTvarka { get; set; }

    public Kelias(int kelias1, int kelias2, bool arPazymejo, int eilesTvarka)
    {
        Kelias1 = kelias1;
        Kelias2 = kelias2;
        ArPazymejo = arPazymejo;
        EilesTvarka = eilesTvarka;
    }
}


/// <summary>
/// Kelių konteinerinė klasė
/// </summary>
class KeliuKonteineris
{
    private Kelias[] Keliai { get; set; }
    public int Skaicius = 1;

    /// <summary>
    /// Konteinerio konstruktorius
    /// </summary>
    /// <param name="dydis"></param>
    public KeliuKonteineris(int dydis)
    {
        Keliai = new Kelias[dydis];
    }

    /// <summary>
    /// Metodas pridedantis kelią į konteinerį
    /// </summary>
    /// <param name="kelias"></param>
    public void PridetiKelia(Kelias kelias)
    {
        Keliai[Skaicius++] = kelias;
    }


    public Kelias GautiKelia(int index)
    {
        return Keliai[index];
    }


    /// <summary>
    /// Metodas pakeičiantis/nustatantis kelio reikšmę konteineryje
    /// </summary>
    /// <param name="index"></param>
    /// <param name="kelias"></param>
    public void NustatytiKelia(int index, Kelias kelias)
    {
        Keliai[index] = kelias;
    }
}

