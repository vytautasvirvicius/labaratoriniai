﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UnixHate.Models.University.LD1_1
{
    public class Map
    {
        public string FileName { get; set; }
        public int Cities { get; set; }
        public int StartingCity { get; set; }
        public List<List<int>> connections = new List<List<int>>();
        public List<int> edgesUsed = new List<int>();

        public void ReadData()
        {
            StreamReader file = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(FileName));
            int cities = int.Parse(file.ReadLine().Split(' ')[0]);
            while (!file.EndOfStream)
            {
                string[] lines = file.ReadLine().Split(' ');
                connections[int.Parse(lines[0])].Add(int.Parse(lines[1]));
            }
            file.Close();
        }

        public void Set()
        {
            CreateDefaultConnections();
            ReadData();
        }

        public void Start()
        {
            TakeStep(1, 0);
        }

        public void CreateDefaultConnections()
        {
            var tempLow = 0;
            for (int i = 0; i < Cities * 4; i++)
            {
                var edges = new List<int>();
                for (int j = tempLow; j % 4 != 0; j++)
                    if (j != i)
                        edges.Add(j + 1);
                connections.Add(edges);
                if (i % 4 == 0)
                    tempLow = i++;
            }
        }

        public int TakeStep(int current, int iteration)
        {
            if (iteration == Cities * 4)
                return current;
            iteration++;
            for (int i = 0; i < 4; i++)
            {
                foreach(var item in edgesUsed)
                    if (item == connections[current][i])
                    {
                        edgesUsed.Remove(edgesUsed.Last());
                        return 0;
                    }
                edgesUsed.Add(current);
                TakeStep(connections[current][i], iteration);
            }
            return 0;
        }
    }
}