﻿using System;

namespace UnixHate.Models.University.LD4_1
{
    public class Transaction : IComparable<Transaction>, IEquatable<Transaction>
    {
        public string BankName { get; set; }
        public string Receiver { get; set; }
        public double Money { get; set; }

        public Transaction(string bankName, string receiver, double money)
        {
            if (bankName == null) throw new ArgumentNullException(nameof(bankName));
            if (receiver == null) throw new ArgumentNullException(nameof(receiver));
            BankName = bankName;
            Receiver = receiver;
            Money = money;
        }


        public int CompareTo(Transaction other)
        {
            throw new NotImplementedException();
        }

        public bool Equals(Transaction other)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return $"{BankName};{Receiver};{Money}\n";
        }
    }
}