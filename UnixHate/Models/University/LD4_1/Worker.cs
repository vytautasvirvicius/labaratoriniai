﻿using System;

namespace UnixHate.Models.University.LD4_1
{
    //pavardė, vardas, banko pavadinimas, sąskaitos Nr.
    public class Worker : IComparable<Worker>, IEquatable<Worker>
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string AccountId { get; set; }
        private double Salary { get; set; }

        public Worker(string surname, string name, string bankName, string accountId)
        {
            if (surname == null) throw new ArgumentNullException(nameof(surname));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (bankName == null) throw new ArgumentNullException(nameof(bankName));
            if (accountId == null) throw new ArgumentNullException(nameof(accountId));
            Surname = surname;
            Name = name;
            BankName = bankName;
            AccountId = accountId;
            Salary = 0;
        }

        public void setSalary(double salary)
        {
            Salary = salary;
        }

        public double getSalary()
        {
            return Salary;
        }

        public int CompareTo(Worker other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var surnameComparison = string.Compare(Surname, other.Surname, StringComparison.Ordinal);
            if (surnameComparison != 0) return surnameComparison;
            var nameComparison = string.Compare(Name, other.Name, StringComparison.Ordinal);
            if (nameComparison != 0) return nameComparison;
            var bankNameComparison = string.Compare(BankName, other.BankName, StringComparison.Ordinal);
            if (bankNameComparison != 0) return bankNameComparison;
            var accountIdComparison = string.Compare(AccountId, other.AccountId, StringComparison.Ordinal);
            if (accountIdComparison != 0) return accountIdComparison;
            return Salary.CompareTo(other.Salary);
        }

        public bool Equals(Worker other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Surname, other.Surname) && string.Equals(Name, other.Name)
                   && string.Equals(BankName, other.BankName) && string.Equals(AccountId,
                       other.AccountId) && Salary.Equals(other.Salary);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Worker) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Surname != null ? Surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (BankName != null ? BankName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (AccountId != null ? AccountId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Salary.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"{Surname};{Name};{BankName};{AccountId};{Salary}\n";
        }


    }
}