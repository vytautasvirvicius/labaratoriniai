﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace UnixHate.Models.University.LD4_1
{
    public class DatabaseLD4_1
    {
        private readonly MySqlConnection _connection = new MySqlConnection(System.Configuration.ConfigurationManager
            .ConnectionStrings["user_information"]
            .ConnectionString);
        private readonly DatabaseErrors _errors = new DatabaseErrors();

        public void AddItemToDb(Item item)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "INSERT INTO Items (Items) VALUES (@item)";
            command.Parameters.AddWithValue("@item", item.ToString());
            command.ExecuteNonQuery();
            _connection.Close();
        }

        public void AddTransactionToDb(Transaction transaction)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "INSERT INTO Transactions (Transactions) VALUES (@transaction)";
            command.Parameters.AddWithValue("@transaction", transaction.ToString());
            command.ExecuteNonQuery();
            _connection.Close();
        }

        public void AddWorkerToDb(Worker worker)
        {
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "INSERT INTO Workers (Workers) VALUES (@worker)";
            command.Parameters.AddWithValue("@worker", worker.ToString());
            command.ExecuteNonQuery();
            _connection.Close();
        }

        public void DeleteItemData()
        {
            _connection.Open();
            MySqlCommand truncate = _connection.CreateCommand();
            truncate.CommandText = "TRUNCATE TABLE Items";
            truncate.ExecuteNonQuery();
            _connection.Close();
        }

        public void DeleteWorkerData()
        {
            _connection.Open();
            MySqlCommand truncate = _connection.CreateCommand();
            truncate.CommandText = "TRUNCATE TABLE Workers";
            truncate.ExecuteNonQuery();
            _connection.Close();
        }

        public void DeleteTransactionData()
        {
            _connection.Open();
            MySqlCommand truncate = _connection.CreateCommand();
            truncate.CommandText = "TRUNCATE TABLE Transactions";
            truncate.ExecuteNonQuery();
            _connection.Close();
        }

        public void SendItemListToDb(List<Item> items)
        {
            foreach(var item in items)
                AddItemToDb(item);
        }

        public void SendWorkerListToDb(List<Worker> workers)
        {
            foreach(var item in workers)
                AddWorkerToDb(item);

        }

        public void SendTransactionListToDb(List<Transaction> transactions)
        {
              foreach(var item in transactions)
                  AddTransactionToDb(item);
        }

        public List<Item> GetItemsFromDb()
        {
            List<Item> items = new List<Item>();
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM Items";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string[] lines = reader.GetString("Items").Split(';');
                items.Add(new Item(lines[0], lines[1], lines[2], int.Parse(lines[3]), double.Parse(lines[4])));
            }
            _connection.Close();
            return items;
        }

        public List<Worker> GetWorkersFromDb()
        {
            List<Worker> workers = new List<Worker>();
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM Workers";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string[] lines = reader.GetString("Workers").Split(';');
                workers.Add(new Worker(lines[0], lines[1], lines[2], lines[3]));
            }
            _connection.Close();
            return workers;
        }

        public List<Transaction> GetTransactionsFromDb()
        {
            List<Transaction> transactions = new List<Transaction>();
            _connection.Open();
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT * FROM Transactions";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string[] lines = reader.GetString("Transactions").Split(';');
                transactions.Add(new Transaction(lines[0], lines[1], double.Parse(lines[2])));
            }
            _connection.Close();
            return transactions;
        }
    }
}